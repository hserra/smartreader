from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.models import User

from feeds.models import Feed, UserProfile


class UserProfileInline(admin.TabularInline):
    model = UserProfile
    can_delete = False
    fields = ['profile', 'feed_list']


class UserAdmin(AuthUserAdmin):
    inlines = (UserProfileInline,)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Feed)
