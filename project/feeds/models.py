from django.db import models
from django.contrib.auth.models import User


class OptionsManager(models.Manager):

    def get_options(self):
        options = Options.objects.all()
        if options:
            options = options[0]
        else:  # Create options row with default values
            options = Options.objects.create()
        return options


class Options(models.Model):

    """
    Options controlling feed reader behavior

    :Fields:

        number_initially_displayed : integer
            Number of entries, from all feeds, initially displayed on webpage.
        number_additionally_displayed : integer
            Number of entries added to displayed results when scrolling down.
        max_entries_saved : integer
            Maximum number of entries to store for each feed.
    """
    number_initially_displayed = models.IntegerField(default=10)
    number_additionally_displayed = models.IntegerField(default=5)
    max_entries_saved = models.IntegerField(default=100)

    objects = models.Manager()
    manager = OptionsManager()

    class Meta:
        verbose_name_plural = "options"

    def __unicode__(self):
        return u'Options'


class Profile(models.Model):

    # TODO: figure out how should the profiles be built

    def __str__(self):
        return super().__str__()

    def get_profile(self):
        return self


class KeyVal(models.Model):
    container = models.ForeignKey(Profile)
    key = models.CharField(max_length=255)
    value = models.IntegerField()


class Feed(models.Model):
    title = models.CharField(max_length=255)
    category = models.CharField(max_length=255, blank=True, null=True)
    category_slug = models.SlugField(blank=True, null=True)
    url = models.URLField(blank=True, null=True)
    profile = models.OneToOneField(Profile)
    etag = models.CharField(max_length=255, blank=True, null=True)
    date_modified = models.DateTimeField(auto_now_add=True)
    xml_url = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title',)

    def num_unread_entries(self):
        return Entry.objects.filter(feed=self, read_flag=False).count()


class Entry(models.Model):
    feed = models.ForeignKey(Feed)
    date_published = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=1023)
    profile = models.OneToOneField(Profile)
    id = models.CharField(max_length=511, primary_key=True)
    author = models.CharField(max_length=255, blank=True, null=True)
    target_url = models.URLField()

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('date_published',)
        verbose_name_plural = 'entries'


class UserProfile(models.Model):
    auth_user = models.OneToOneField(User)
    profile = models.OneToOneField(Profile)
    feed_list = models.ManyToManyField(Feed)
    entries_in_feed = models.ManyToManyField(Entry, through='UserEntryList', through_fields=(
        'user_id', 'entry_id'), related_name='entries')

    def __str__(self):
        return self.auth_user.username

    # TODO: username not working

    def unread_posts(self):
        return self.entries.filter(is_read=False)

    def unread_count(self):
        return self.unread_posts().count()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        profile = Profile()
        profile.save()
        self.profile = profile
        super(UserProfile, self).save()


class UserEntryList(models.Model):
    user_id = models.OneToOneField(UserProfile, related_name='user')
    entry_id = models.OneToOneField(Entry, related_name='entry')
    is_read = models.BooleanField(default=False)
    is_favorite = models.BooleanField(default=False)
    is_saved = models.BooleanField(default=False)
