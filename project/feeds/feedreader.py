__author__ = 'hugoserra'

import logging
from datetime import datetime
from time import mktime

from django.utils import timezone, html
import feedparser
import pytz

from smartreader import settings
from .models import Entry, Options

logger = logging.getLogger('smartreader')


def gen_profile_score(to_score):
    # TODO: Get text from object to_score and generate and return the score
    return


def update_feed(db_feed, verbose=False):
    """
    Read through a feed looking for new entries.
    """

    url = db_feed.xml_url
    etag = db_feed.etag
    last_updates = db_feed.date_modified

    options = Options.manager.get_options()
    parsed = feedparser.parse(url, etag=etag, modified=last_updates)

    # Check if the feed is malformed
    if hasattr(parsed.feed, 'bozo_exception'):
        msg = 'Feedreader poll_feeds found Malformed feed, %s: %s' % (db_feed.xml_url, parsed.feed.bozo_exception)
        logger.warning(msg)
        if verbose:
            print(msg)
        return

    # Get the date when the feed was published, then convert to local time
    if hasattr(parsed.feed, 'published_parsed'):
        published_time = datetime.fromtimestamp(mktime(parsed.feed.published_parsed))
        published_time = pytz.timezone(settings.TIME_ZONE).localize(published_time, is_dst=None)
        if db_feed.published_time and db_feed.published_time >= published_time:
            return
        db_feed.published_time = published_time

    # Check if the feed has all the details
    for attr in ['title', 'title_detail', 'link', 'description', 'description_detail']:
        if not hasattr(parsed.feed, attr):
            msg = 'Feedreader poll_feeds. Feed "%s" has no %s' % (db_feed.xml_url, attr)
            logger.error(msg)
            if verbose:
                print(msg)
            return

    # Fill in the feed properties, escaping html tags
    if parsed.feed.title_detail.type == 'text/plain':
        db_feed.title = html.escape(parsed.feed.title)
    else:
        db_feed.title = parsed.feed.title
    db_feed.link = parsed.feed.link
    if parsed.feed.description_detail.type == 'text/plain':
        db_feed.description = html.escape(parsed.feed.description)
    else:
        db_feed.description = parsed.feed.description
    db_feed.last_polled_time = timezone.now()
    db_feed.save()

    # Process each entry for this feed
    if verbose:
        print('%d entries to process in %s' % (len(parsed.entries), db_feed.title))
    for i, entry in enumerate(parsed.entries):
        if i >= options.max_entries_saved:
            break
        missing_attr = False
        for attr in ['title', 'title_detail', 'link', 'description']:
            if not hasattr(entry, attr):
                msg = 'Feedreader poll_feeds. Entry "%s" has no %s' % (entry.link, attr)
                logger.error(msg)
                if verbose:
                    print(msg)
                missing_attr = True
        if missing_attr:
            continue
        if entry.title == "":
            msg = 'Feedreader poll_feeds. Entry "%s" has a blank title' % entry.link
            if verbose:
                print(msg)
            logger.warning(msg)
            continue
        db_entry, created = Entry.objects.get_or_create(feed=db_feed, target_url=entry.link)
        if created:
            if hasattr(entry, 'published_parsed'):
                published_time = datetime.fromtimestamp(mktime(entry.published_parsed))
                published_time = pytz.timezone(settings.TIME_ZONE).localize(published_time, is_dst=None)
                now = timezone.now()
                if published_time > now:
                    published_time = now
                db_entry.date_published = published_time
            if entry.title_detail.type == 'text/plain':
                db_entry.title = html.escape(entry.title)
            else:
                db_entry.title = entry.title
            # Lots of entries are missing description_detail attributes. Escape their content by default
            if hasattr(entry, 'description_detail') and entry.description_detail.type != 'text/plain':
                db_entry.description = entry.description
            else:
                db_entry.description = html.escape(entry.description)
            db_entry.save()
