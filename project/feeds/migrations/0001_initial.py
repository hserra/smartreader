# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('date_published', models.DateTimeField(auto_now_add=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=1023)),
                ('id', models.CharField(serialize=False, max_length=511, primary_key=True)),
                ('author', models.CharField(null=True, max_length=255, blank=True)),
                ('target_url', models.URLField()),
            ],
            options={
                'verbose_name_plural': 'entries',
                'ordering': ('date_published',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Feed',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('category', models.CharField(null=True, max_length=255, blank=True)),
                ('category_slug', models.SlugField(null=True, blank=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('etag', models.CharField(null=True, max_length=255, blank=True)),
                ('date_modified', models.DateTimeField(auto_now_add=True)),
                ('xml_url', models.CharField(max_length=255, unique=True)),
            ],
            options={
                'ordering': ('title',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Options',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('number_initially_displayed', models.IntegerField(default=10)),
                ('number_additionally_displayed', models.IntegerField(default=5)),
                ('max_entries_saved', models.IntegerField(default=100)),
            ],
            options={
                'verbose_name_plural': 'options',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserEntryList',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('is_read', models.BooleanField(default=False)),
                ('is_favorite', models.BooleanField(default=False)),
                ('is_saved', models.BooleanField(default=False)),
                ('entry_id', models.OneToOneField(related_name='entry', to='feeds.Entry')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=255, unique=True)),
                ('username_slug', models.SlugField()),
                ('auth_user', models.ForeignKey(unique=True, to=settings.AUTH_USER_MODEL)),
                ('entries_in_feed',
                 models.ManyToManyField(related_name='entries', to='feeds.Entry', through='feeds.UserEntryList')),
                ('feed_list', models.ManyToManyField(to='feeds.Feed')),
                ('profile', models.OneToOneField(to='feeds.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='userentrylist',
            name='user_id',
            field=models.OneToOneField(related_name='user', to='feeds.UserProfile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='feed',
            name='profile',
            field=models.OneToOneField(to='feeds.Profile'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='entry',
            name='feed',
            field=models.ForeignKey(to='feeds.Feed'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='entry',
            name='profile',
            field=models.OneToOneField(to='feeds.Profile'),
            preserve_default=True,
        ),
    ]
