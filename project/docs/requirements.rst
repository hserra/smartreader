Requirements
============

SmartReader requires the following modules but simply installing it
using Pip_ [#]_ will also install them. Just type::

    pip install django-planet


Required
--------

* Python 3.3+
* Django 1.7+
* feedparser
* pytz
* BeautifulSoup


.. _Pip: http://pip.openplans.org/
.. [#] *Don't forget to use the correct pip instance for your version of Python.*