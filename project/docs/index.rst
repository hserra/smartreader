.. SmartReader documentation master file, created by
sphinx-quickstart on Wed Mar 25 16:10:50 2015.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.

Welcome to SmartReader's documentation!
=======================================

Contents:

.. toctree::
:maxdepth: 2

       requirements


Getting Help
============

There are two primary ways of getting help. We have a `mailing list`_ hosted at
Google (https://groups.google.com/forum/#!forum/django-planet) or you may contact us
via email to matagus at gmail dot com. You may also `open an issue`_ in our
github repository (it requires you to have a github account).

.. _`mailing list`: https://groups.google.com/forum/#!forum/django-planet
.. _`open an issue`: https://github.com/matagus/django-planet/issues/




Why django-planet?
==================

There are other feed aggregators out there for Django. You need to assess
the options available and decide for yourself. That said, here are some
common reasons for django-planet.

* You need to quickly create a blog aggregator website with a nice look & feel.
* You want a full website for browsing blog posts and its authors and tags,
  feeds and blogs.
* SEO matters to you: django-planet has templates with SEO metatgas and it
  includes sitemaps so you may submit them to your favorite search engines.
* You want searching posts, blogs, tags and authors.
* You need to customize templates and have a rich set of template tags to do it.
* You want complete ATOM & RSS support

Running The Tests
=================

The easiest way to get setup to run django-planet's tests looks like::

  $ git clone https://github.com/matagus/django-planet.git
  $ cd django-planet
  $ virtualenv env
  $ . env/bin/activate
  $ ./env/bin/pip install -U -r requirements.txt
  $ ./env/bin/pip install -U mock django-discover-runner factory-boy tox

Then running the tests is as simple as::

  # From the same directory as above:
  $ tox

That will test django-planet using python 2.7 combinated with Django 1.4,
Django 1.5 and Django 1.6.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

