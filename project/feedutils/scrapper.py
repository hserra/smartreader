import re

__author__ = 'hugoserra'

import urllib.request as urllib
from bs4 import BeautifulSoup
from newspaper import Article
# import pprint

if __name__ == '__main__':
    # html = urllib.urlopen('http://lifehacker.com/how-to-write-with-substance-and-improve-your-communicat-1698241167').read()
    # soup = BeautifulSoup(html)
    # texts = soup.findAll(text=True)
    # visible_texts = filter(visible, texts)
    # print(texts)

    url = 'http://lifehacker.com/how-to-write-with-substance-and-improve-your-communicat-1698241167'
    article = Article(url)
    article.download()
    # pp = pprint.PrettyPrinter(indent=4)
    # pp.pprint(article.html)
    article.parse()
    print(article.authors)
    print(article.text)
    print(article.top_image)
