import math

__author__ = 'hugoserra'

import feedparser
import sqlite3
import hashlib
from textblob import TextBlob
from newspaper import Article
import pprint
import sys


def get_all_feeds():
    feeds_urls = ["http://feeds.gawker.com/lifehacker/full"]

    return feeds_urls


def parse_feeds(feeds_urls):
    p_feeds = []

    for feed in feeds_urls:
        parsed_feed = feedparser.parse(feed)
        p_feeds.append(parsed_feed)

    return p_feeds


def prepare_feeds(feeds):
    feeds_for_db = []

    for feed in feeds:
        hasher = hashlib.md5()
        title = feed['feed']['title']
        hasher.update(title.encode('utf-8'))
        id = hasher.digest()
        url = feed['href']
        feeds_for_db.append({'id': id, 'url': url, 'title': title})

    return feeds_for_db


def create_test_db():
    try:
        con = sqlite3.connect('test.db')

        with con:

            cur = con.cursor()
            cur.execute("CREATE TABLE Feeds(id INT, title TEXT, url TEXT)")
            cur.execute("CREATE TABLE Entries(id INT, feed_id INT, title TEXT, description TEXT)")
            cur.close()

        con.close()
    finally:
        return

    return


def write_feeds(feeds):
    try:
        con = sqlite3.connect('test.db')

        with con:

            cur = con.cursor()
            for feed in feeds:
                cur.execute("INSERT INTO Feeds VALUES(?, ?, ?)", (feed['id'], feed['title'], feed['url']))

    finally:
        print("Ended.")

    return


def get_entries(feeds):
    for feed in feeds:
        return feed.entries


def get_tf_idf(text):
    bloblist = [TextBlob(text)]
    for i, blob in enumerate(bloblist):
        print("Top words in document {}".format(i + 1))
        scores = {word: tfidf(word, blob, bloblist) for word in blob.words}
        sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
        for word, score in sorted_words:
            print("\tWord: {}, TF-IDF: {}".format(word, round(score, 5)))


def tf(word, blob):
    return blob.words.count(word) / len(blob.words)


def n_containing(word, bloblist):
    return sum(1 for blob in bloblist if word in blob)


def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))


def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)


if __name__ == '__main__':
    urls = get_all_feeds()
    parsed_feeds = parse_feeds(urls)
    new_entries = get_entries(parsed_feeds)
    for entry_item in new_entries:
        entry_id = entry_item.id
        entry_name = entry_item.title
        if 'content' in entry_item.keys():
            entry_content = entry_item.content
            print('\tContent: ' + entry_content)
        entry_summary = entry_item.summary
        entry_summary_detail = entry_item.summary_detail
        entry_url = entry_item.link
        print('Entry id:' + entry_id)
        print('\tTitle: ' + entry_name)
        print('\tLink: ' + entry_url)
        print('\tSummary: ')
        print('\t\t' + entry_summary)
        print('\n')
        # sys.stdout.write('\t\tDetail: ')
        # pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(entry_summary_detail)
        # print('\t\tDetail: ' + entry_summary_detail)
        article = Article(entry_url)
        article.download()
        # pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(article.html)
        article.parse()
        print(article.authors)
        print(article.text)
        print(article.top_image)
        print('\n')
        get_tf_idf(article.text)
    # pp = pprint.PrettyPrinter(indent=4)
    # pp.pprint(new_entries)
    print('end;')
