from django.conf.urls import patterns, include, url
from django.contrib import admin

from feeds import views

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'smartreader.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^register/$', views.register, name='register'),  # ADD NEW PATTERN!
                       url(r'^', views.index)
                       )
