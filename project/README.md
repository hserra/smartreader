README
======

SmartReader is a feed reader/aggregator with smart sorting and recommendation of entries written in Django 1.7.

Features
--------

* Locally stored feed link, title and description.
* Locally stored entry link, title and description.
* Show recent entries.
* String search of locally stored data.
* Sort entries according to the users preferences.
* Sugest feeds based on user preferences.

Dependencies
------------

*  [`Python 3.4.3`](https://www.python.org/downloads/release/python-343/)
*  [`Django 1.7.7`](https://pypi.python.org/pypi/Django/1.7.7)
*  [`feedparser 5.1.3`](https://pypi.python.org/pypi/feedparser/5.1.3)
*  [`pytz 2015.2`](https://pypi.python.org/pypi/pytz/2015.2)

Upgrading packages:

    pip3 freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 sudo pip3 install -U