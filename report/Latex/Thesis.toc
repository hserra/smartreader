\select@language {english}
\contentsline {section}{Acknowledgments}{v}{section*.1}
\contentsline {section}{Resumo}{vii}{section*.2}
\contentsline {section}{Abstract}{ix}{section*.3}
\contentsline {section}{List of Tables}{xiii}{chapter*.5}
\contentsline {section}{List of Figures}{xv}{chapter*.6}
\contentsline {section}{Nomenclature}{xvii}{chapter*.7}
\contentsline {section}{Glossary}{xix}{table.0.1}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}State-of-the-art}{1}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Tables}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Drawings}{2}{subsection.1.2.2}
\contentsline {chapter}{\numberline {2}Results}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Figures}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Equations}{4}{section.2.2}
\contentsline {chapter}{\numberline {3}Conclusions}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Achievements}{5}{section.3.1}
\contentsline {section}{\numberline {3.2}Future Work}{5}{section.3.2}
\contentsline {chapter}{Bibliography}{7}{chapter*.9}
\contentsline {chapter}{\numberline {A}Vector calculus}{9}{appendix.A}
\contentsline {section}{\numberline {A.1}Vector identities}{9}{section.A.1}
